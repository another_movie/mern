import mongoose from "mongoose";

const UserScheme = mongoose.Schema(
  {
    firstName: { type: "String", required: true, min: 2, max: 50 },
    lastName: { type: "String", required: true, min: 2, max: 50 },
    email: { type: String, unique: true, min: 2, max: 50 },
    password: { type: String, required: true },
    friends: { type: Array, default: [] },
    picturePath: { type: String, default: "" },
    location: String,
    occupation: String,
    viewedProfile: Number,
    impressions: Number,
  },
  {
    timestamp: true,
  }
);

export default mongoose.model("User", UserScheme);

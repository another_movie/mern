import express from "express";
import {
  getUser,
  getUserFriends,
  addRemoveFriend,
} from "../controllers/users.js";
import { verifyToken } from "../middleware/auth.js";

const route = express.Router();

route.get("/:id", verifyToken, getUser);
route.get("/:id/friends", verifyToken, getUserFriends);

route.patch("/:id/:friendId", verifyToken, addRemoveFriend);

export default route;
